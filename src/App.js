import React, { Component } from "react";

import Titles from "./components/Titles";
import Form from "./components/Form";
import Weather from "./components/Weather";

const API_KEY = "acb1d8952af055d0a2443dbc493aa5fa";

class App extends Component {
  state = {
    city: undefined,
    country: undefined,
    temprature: undefined,
    humidity: undefined,
    pressure: undefined,
    description: undefined,
    error: undefined
  };

  getWeather = async e => {
    e.preventDefault();

    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;

    const apiCall = await fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`
    );
    const data = await apiCall.json();

    if (city && country) {
      this.setState({
        city: data.name,
        country: data.sys.country,
        temprature: data.main.temp,
        humidity: data.main.humidity,
        pressure: data.main.pressure,
        description: data.weather[0].description,
      });
    } else {
      this.setState({
        error: 'Please enter the values!'
      });
    }
  };

  render() {
    return (
      <div className="App">
        <Titles />
        <Form getWeather={this.getWeather} />
        <Weather
          city={this.state.city}
          country={this.state.country}
          temp={this.state.temprature}
          humidity={this.state.humidity}
          pressure={this.state.pressure}
          description={this.state.description}
          error={this.state.error}
        />
      </div>
    );
  }
}

export default App;
